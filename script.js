function autoType(id, text, speed) {
  var element = document.getElementById(id),
    txt = text.join('').split('');

  var interval = setInterval(
    function () {
      if (!txt[0]) {
        return clearInterval(interval);
      }
      
      element.innerText += txt.shift(); // innerHTML if you need to insert HTML code
    },
    speed != undefined ? speed : 100
  );

  return false;
}

document.addEventListener('DOMContentLoaded', function () {
  autoType('one', [
    'Wake up Neo ...\n',
    'The Matrix has you ...\n',
    'Follow the white rabbit ...\n',
    'Knock knock Neo.',
    '\n',
    '\n',
    'And now about the serious\n',
    '\n',
    '===============================\n',
    '=  ====  =============  ====  =\n',
    '=  ====  =============  ====  =\n',
    '=  ====  ==  =   =  ==  ====  =\n',
    '=  ====  ==  =   =  ==  ====  =\n',
    '=  ====  ===   =   ===  ====  =\n',
    '=  ====  ===   =   ===  ====  =\n',
    '=   ==   =============   ==   =\n',
    '==      ===============      ==\n',
    '===============================\n',
    '\n',
    'uwu, also stylized as UwU, is \n',
    'an emoticon indicating cuteness.\n',
    'The "u" characters represent\n',
    'eyes, while the "w" represents\n',
    'a mouth. uwu is often used to\n',
    'denote cuteness, happiness, or\n',
    'tenderness. Excessive usage of\n',
    'the emoticon can also have the\n',
    'intended effect of annoying its\n',
    'recipient.',
    '\n',
    '\n',
    'GPG/PGP:\n 0xE5A50C7D0EFEB8D4\n',
    '\n',
    'jabber/xmpp: zoya@5222.de\n',
    'jabber/xmpp: uwu@anonym.im\n',
    '\n',
    'In extreme cases, write to e-mail\n',
    'e-mail: kornyshova_zoya@mail.ru',
  ], 50);
});
