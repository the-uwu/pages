# UwU.rs

![uwu](Stylized_uwu_emoticon.svg.png)

**uwu**, also stylized as **UwU**, is an emoticon indicating cuteness. The **"u"** characters represent eyes, while the **"w"** represents a mouth.

**uwu** is often used to denote cuteness, happiness, or tenderness. Excessive usage of the emoticon can also have the intended effect of annoying its recipient.

[See on Wikipedia](https://en.wikipedia.org/wiki/Uwu)